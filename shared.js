const backdrop = document.querySelector('.backdrop');
const modal = document.querySelector('.modal');
const selectPlanButons = [...document.querySelectorAll('.plan .button')];
const noPlanButton = document.querySelector('.modal__action--negative');
const toggleButton = document.querySelector('.toggle-button');
const mobileNav = document.querySelector('.mobile-nav');
const ctaButton = document.querySelector('.main-nav__item--cta');

const BACKDROP_TIMEOUT = 200;

selectPlanButons.map(planButton => {
  planButton.addEventListener('click', () => {
    // modal.style.display = 'block';
    // backdrop.style.display = 'block';
    modal.classList.add('open');
    backdrop.style.display = 'block';
    setTimeout(() => backdrop.classList.add('open'), 0);
  })
});

const closeModal = () => {
  // modal.style.display = 'none';
  // backdrop.style.display = 'none';
  if (modal) {
    modal.classList.remove('open');
  }
  backdrop.classList.remove('open');
  setTimeout(() => backdrop.style.display = 'none', BACKDROP_TIMEOUT);
};

if (noPlanButton) {
  noPlanButton.addEventListener('click', closeModal);
}

backdrop.addEventListener('click', () => {
  closeModal();
  // mobileNav.style.display = 'none';
  
  mobileNav.classList.remove('mob-nav__open');
  // mobileNav.classList.remove('open');
  // setTimeout(() => {
  //   mobileNav.classList.remove('mob-nav__open');
  // }, BACKDROP_TIMEOUT);
});

toggleButton.addEventListener('click', () => {
  // mobileNav.style.display = 'block';
  // backdrop.style.display = 'block';
  // mobileNav.classList.add('open');
  backdrop.style.display = 'block';
  setTimeout(() => {
    backdrop.classList.add('open');
    mobileNav.classList.add('mob-nav__open');
  }, 0);
});

ctaButton.addEventListener('animationstart', (event) => {
  console.log('Animation started', event);
});

ctaButton.addEventListener('animationend', (event) => {
  console.log('Animation ended', event);
});

ctaButton.addEventListener('animationiteration', (event) => {
  console.log('Animation iterated', event);
});
